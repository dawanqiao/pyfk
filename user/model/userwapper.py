from jdbcdao.iwapper import IWapper
from user.model.user import User


class UserWapper(IWapper):

    def wapper(self, row):
        return User(row[0], row[1],row[2])