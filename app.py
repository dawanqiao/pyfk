from flask import Flask, redirect, url_for, request, render_template, make_response, session
import os

app = Flask(__name__)
app.secret_key = '123456'
app.config['UPLOAD_FOLDER'] = 'd:/upload'
app.config['MAX_CONTENT_PATH'] = 1024*1024*1024*10 #10M
@app.route('/')
def hello_world():
    msg = "in method"
    print(msg)
    return 'Hello World!'


@app.route('/index')
def index():
    login_name = session.get('username')
    if login_name is None:
        return render_template('login.html')
    else:
        return "this is index page"


@app.route("/login", methods=['post'])
def login():
    username = request.form['username']
    pwd = request.form['pwd']
    if username.strip() == '' or pwd.strip() == '':
        return render_template('login.html', msg='用户名和密码不能为空！')
    else:
        session['username'] = username
        return '登录成功'


@app.route('/upload', methods=['get', 'post'])
def upload():
    m = request.method
    if m.lower() == 'get':
        return render_template('upload.html')
    else:
        upload_file = request.files.get('file01')
        print(upload_file)
        filepath = os.path.join('static', upload_file.filename)
        upload_file.save(filepath)
        return 'upload success'


@app.route("/404")
def not_found():
    return 'dispatch to get page '


@app.route('/dispatch/<action>')
def dispatch(action):
    if action == 'list':
        return redirect(url_for("list"))
    elif action == 'get':
        return redirect(url_for('get'))
    else:
        return redirect(url_for('not_found'))


if __name__ == '__main__':
    app.run(debug=True)
